module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            dist: {
                files: {
                    'dist/dataview.sqlite.source.min.js': ['index.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('addlinefeed', 'Add Line Feed', function() {
        var fs = require('fs');
        fs.appendFileSync('dist/dataview.sqlite.source.min.js', "\n");
    });

    grunt.registerTask('default', 'Default Tasks', function() {
        grunt.task.run(['uglify:dist', 'addlinefeed']);
    });
};
