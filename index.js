(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('SqliteSource', ['qw'], factory);
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(require('qw'));
    } else {
        root.SqliteSource = factory(root.qw);
    }
})(this, function (qw) {
    function SqliteSource()
    {
        if (!(this instanceof SqliteSource)) {
            return new SqliteSource();
        }
        this._dataview = null;
        this._sql_builder_fn = null;
    }
    SqliteSource.create = function() {
        return new SqliteSource();
    };
    SqliteSource.prototype.destroy = function() {
        delete this._dataview;
        delete this._sql_builder_fn;
    };
    SqliteSource.prototype.setDataView = function(dataview) {
        this._dataview = dataview;
        return this;
    };
    SqliteSource.prototype.setSqlBuilder = function(callback) {
        this._sql_builder_fn = qw.isFunction(callback) ? callback : null;
        return this;
    };
    SqliteSource.prototype.request = function(request_data) {
        var dataview = this._dataview,
            query, count_query, first_row = true,
            extraData = dataview.getOption('extraData'),
            params = {
                search: null,
                orderBy: null,
                orderType: 'ASC',
                page: 1,
                perPage: 10
            };

        if (!qw.isPlainObject(request_data)) {
            request_data = {};
        }

        if (qw.isPlainObject(extraData)) {
            qw.extend(request_data, extraData);
        }

        if (qw.hasOwn(request_data, 'search') && qw.isString(request_data.search)) {
            var search = request_data.search.trim();
            if (search !== '') {
                params.search = search;
            }
        }

        if (qw.hasOwn(request_data, 'orderBy') && qw.isString(request_data.orderBy)) {
            var orderBy = request_data.orderBy.trim();
            if (orderBy !== '') {
                params.orderBy = orderBy;
            }
        }

        if (params.orderBy && qw.hasOwn(request_data, 'orderType') && qw.isString(request_data.orderType)) {
            var orderType = request_data.orderType.trim().toUpperCase();
            params.orderType = orderType !== 'ASC' ? 'DESC' : 'ASC';
        }

        if (qw.hasOwn(request_data, 'page') && (qw.isNumber(request_data.page) || request_data.page.match(/^\d+$/))) {
            params.page = request_data.page;
        }

        if (qw.hasOwn(request_data, 'perPage') && (qw.isNumber(request_data.perPage) || request_data.perPage.match(/^\d+$/))) {
            params.perPage = request_data.perPage;
        }

        query = this._sql_builder_fn(params);

        dataview.disable();
        // console.log(query);
        global.db.get(query.count_sql, query.count_params, function(err, data) {
            if (err) {
                // console.log('global.db.get', err);
                return;
            }
            params.count = data.row_count;
            // console.log(data.row_count);
            if (params.count == 0) {
                dataview.prepareForResponse(params);
                dataview.enable();
                dataview._request_count++;
            } else {
                global.db.each(query.sql, query.params, function(err, row) {
                    if (err) {
                        // console.log('global.db.each', err);
                        return;
                    }
                    if (first_row) {
                        dataview.prepareForResponse(params);
                    }
                    dataview.addRow(row);
                    first_row = false;
                }, function(err) {
                    dataview.enable();
                    dataview._request_count++;
                });
            }
        });
    };
    return SqliteSource;
});
